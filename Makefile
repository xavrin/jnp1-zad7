CPP = g++
CPPFLAGS = -Wall -std=c++11

all: 42

42: 42.cc
	$(CPP) $(CPPFLAGS) $< -o $@

clean:
	rm 42
