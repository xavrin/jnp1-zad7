#include <stack>
#include <string>
#include <functional>
#include <exception>
#include <cassert>
#include <map>

class SyntaxError : public virtual std::exception {};
class OperatorAlreadyDefined : public virtual std::exception {};
class UnknownOperator : public virtual std::exception {};

typedef std::function<int()> Lazy;

class LazyCalculator {
    private:
        typedef std::function<int(Lazy, Lazy)> Operator;
        std::map<char, Operator> operators;
        std::map<char, Lazy> literals;
    public:
        LazyCalculator()
            : operators {
                {'+', [] (Lazy a, Lazy b) { return a() + b(); }},
                {'-', [] (Lazy a, Lazy b) { return a() - b(); }},
                {'*', [] (Lazy a, Lazy b) { return a() * b(); }},
                {'/', [] (Lazy a, Lazy b) { return a() / b(); }},
            }
        {
            for (char i : {'0', '2', '4'})
                literals[i] = [i] () { return i - '0'; };
        }

        Lazy parse(const std::string& s) const {
            std::stack<Lazy> st;
            for (char c : s) {
                auto lit = literals.find(c);
                if (lit != literals.end())
                    st.push(lit->second);
                else {
                    auto op = operators.find(c);
                    if (op != operators.end()) {
                        if (st.size() < 2)
                            throw SyntaxError();
                        Lazy b = st.top(); st.pop();
                        Lazy a = st.top(); st.pop();
                        st.push(std::bind(op->second, a, b));
                    }
                    else
                        throw UnknownOperator();
                }
            }
            if (st.size() != 1)
                throw SyntaxError();
            return st.top();
        }

        int calculate(const std::string& s) const {
            return parse(s)();
        }

        void define(char c, std::function<int(Lazy, Lazy)> fn) {
            if (operators.count(c) || literals.count(c))
                throw OperatorAlreadyDefined();
            operators[c] = fn;
        }
};

std::function<void(void)> operator*(int n, std::function<void(void)> fn) {
    return [=]() {
       for (int i = 0; i < n; i++)
           fn();
    };
}

int manytimes(Lazy n, Lazy fn) {
    (n() * fn)();  // Did you notice the type cast?
    return 0;
}

int main() {
    LazyCalculator calculator;

    // The only literals...
    assert(calculator.calculate("0") == 0);
    assert(calculator.calculate("2") == 2);
    assert(calculator.calculate("4") == 4);

    // Built-in operators.
    assert(calculator.calculate("42+") == 6);
    assert(calculator.calculate("24-") == -2);
    assert(calculator.calculate("42*") == 8);
    assert(calculator.calculate("42/") == 2);

    assert(calculator.calculate("42-2-") == 0);
    assert(calculator.calculate("242--") == 0);
    assert(calculator.calculate("22+2-2*2/0-") == 2);

    // The fun.
    calculator.define('!', [](Lazy a, Lazy b) { return a()*10 + b(); });
    assert(calculator.calculate("42!") == 42);

    std::string buffer;
    calculator.define(',', [](Lazy a, Lazy b) { a(); return b(); });
    calculator.define('P', [&buffer](Lazy a, Lazy b) { buffer += "pomidor"; return 0; });
    assert(calculator.calculate("42P42P42P42P42P42P42P42P42P42P42P42P42P42P42P42P,,,,42P42P42P42P42P,,,42P,42P,42P42P,,,,42P,,,42P,42P,42P,,42P,,,42P,42P42P42P42P42P42P42P42P,,,42P,42P,42P,,,,,,,,,,,,") == 0);
    assert(buffer.length() == 42 * std::string("pomidor").length());

    std::string buffer2 = std::move(buffer);
    buffer.clear();
    calculator.define('$', manytimes);
    assert(calculator.calculate("42!42P$") == 0);
    // Notice, how std::move worked.
    assert(buffer.length() == 42 * std::string("pomidor").length());

    calculator.define('?', [](Lazy a, Lazy b) { return a() ? b() : 0; });
    assert(calculator.calculate("042P?") == 0);
    assert(buffer == buffer2);

    assert(calculator.calculate("042!42P$?") == 0);
    assert(buffer == buffer2);

    for (auto bad: {"", "42", "4+", "424+"}) {
        try {
            calculator.calculate(bad);
            assert(false);
        } catch (SyntaxError) { }
    }

    try {
        calculator.define('!', [](Lazy a, Lazy b) { return a()*10 + b(); });
        assert(false);
    } catch (OperatorAlreadyDefined) { }

    try {
        calculator.calculate("02&");
        assert(false);
    } catch (UnknownOperator) { }
}
